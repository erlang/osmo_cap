-include_lib("osmo_cap/include/CAP-operationcodes.hrl").

-module(cap_helper).

-export([decode_op/2]).

rec4op(?'opcode-initialDPSMS', arg) -> 			'CapSmsComponentPortion_InitialDPSMSArg';
rec4op(?'opcode-furnishChargingInformationSMS', arg) -> 'CapSmsComponentPortion_FurnishChargingInformationSMSArg';
rec4op(?'opcode-connectSMS', arg) ->			'CapSmsComponentPortion_ConnectSMSArg';
rec4op(?'opcode-requestReportSMSEvent', arg) ->		'CapSmsComponentPortion_RequestReportSMSEventArg';
rec4op(?'opcode-eventReportSMS', arg) -> 		'CapSmsComponentPortion_EventReportSMSArg';
rec4op(?'opcode-continueSMS', _) ->			{error, emptyDataType};
rec4op(?'opcode-releaseSMS', _) ->			{error, emptyDataType};
rec4op(?'opcode-resetTimerSMS', arg) ->			'CapSmsComponentPortion_ResetTimerSMSArg';
rec4op(_, _) ->						{error, unknownOperation}.

decode_op(_, <<>>) ->
	undefined;
decode_op(_, []) ->
	undefined;
decode_op({local, Op}, Enc) when is_binary(Enc) or is_list(Enc) ->
	case rec4op({local, Op}, arg) of
		{error, Err} ->
			{error, Err};
		RecType ->
			'CAP-smsSSF-gsmSCF-pkgs-contracts-acs':decode(RecType, Enc)
	end.
