
-module(camel_srv_fsm).
-behaviour(gen_fsm).

-export([init/1, handle_info/3, handle_event/3, handle_sync_event/4,
		terminate/3, code_change/4]).

-export([start_link/0]).

-export([idle/2, begin_rcvd/2]).

-include_lib("osmo_ss7/include/osmo_ss7.hrl").
-include_lib("osmo_ss7/include/osmo_util.hrl").
-include_lib("osmo_ss7/include/mtp3.hrl").
-include_lib("osmo_ss7/include/sccp.hrl").
-include_lib("TCAP/include/tcap.hrl").
-include_lib("osmo_cap/include/CAP-smsSSF-gsmSCF-pkgs-contracts-acs.hrl").
-include_lib("osmo_cap/include/CAP-datatypes.hrl").

-record(state, {tco, local, remote, acn, dlg}).

start_link() ->
	gen_fsm:start_link(?MODULE, [], [{debug, [trace]}]).

init([]) ->
	ok = application:start(osmo_ss7),
	LocalPC=osmo_util:pointcode2int(itu, {1,2,3}),
	RemotePC=osmo_util:pointcode2int(itu, {1,2,1}),
	ok = ss7_links:register_linkset(LocalPC, RemotePC, "itp"),
	SLlocal = #sigtran_peer{ip={0,0,0,0}, port=60180, point_code=LocalPC},
	SLremote = #sigtran_peer{ip={192,168,104,2}, port=2905, point_code=RemotePC},
	L = #sigtran_link{type=m3ua, name="itp_link1", linkset_name="itp", sls=0, local=SLlocal, remote=SLremote},
	{ok,LPid} = osmo_ss7_sup:add_mtp_link(L),
	ok = ss7_routes:create_route(0,0,"itp"),
	ok = application:start(osmo_sccp),
	ok = application:start(tcap),
	{ok, TCOPid} = tcap_user:start_sap(osmo_sccp_tcap, [146], []),
	gen_server:call(TCOPid, set_usap),
	{ok, idle, #state{tco = TCOPid}}.

idle({'TC','BEGIN',indication,Begin}, State) ->
	#'TC-BEGIN'{destAddress=DA, appContextName=ACN,
		    origAddress=OA, dialogueID=Dlg,
		    userInfo=UserInfo, componentsPresent=CompPres} = Begin,
	{next_state, begin_rcvd, State#state{local=DA, remote=OA, acn=ACN, dlg=Dlg}}.

begin_rcvd({'TC','INVOKE',indication,Inv}, State) ->
	#'TC-INVOKE'{dialogueID=Dlg, invokeID=InvId, operation = {local, 60},
		     parameters = Params} = Inv,

	% generate and encode the RRB
	Rrb = #'Generic-sms-PDUs_RequestReportSMSEventArg'{sMSEvents = [
			#'SMSEvent'{eventTypeSMS = 'o-smsFailure',
				    monitorMode = interrupted},
			#'SMSEvent'{eventTypeSMS = 'o-smsSubmission',
				    monitorMode = interrupted}],
			extensions = asn1_NOVALUE},
	{ok, RrbBin} = 'CAP-smsSSF-gsmSCF-pkgs-contracts-acs':encode('SmsInvokable_RequestReportSMSEventArg', Rrb),
	RrbInv = #'TC-INVOKE'{dialogueID = Dlg, invokeID = 23, operation = {local, 63},
			      class = 2, parameters = RrbBin},
	tcap_user:send_prim(State#state.tco, {'TC','INVOKE',request,RrbInv}),

	RrbCont = #'TC-CONTINUE'{dialogueID=Dlg, appContextName=State#state.acn},
	tcap_user:send_prim(State#state.tco, {'TC','CONTINUE',request,RrbCont}),


	timer:sleep(5000),

	ContInv = #'TC-INVOKE'{dialogueID=Dlg, invokeID = 24,
			       operation = {local, 65}, class = 2},
	tcap_user:send_prim(State#state.tco, {'TC','INVOKE',request,ContInv}),

	ContCont = #'TC-CONTINUE'{dialogueID=Dlg},
	tcap_user:send_prim(State#state.tco, {'TC','CONTINUE',request,ContCont}),


	timer:sleep(5000),

	RelInv = #'TC-INVOKE'{dialogueID=Dlg, invokeID = 25,
				operation = {local, 66}, class = 2},
	tcap_user:send_prim(State#state.tco, {'TC','INVOKE',request,RelInv}),

	End = #'TC-END'{dialogueID=Dlg, termination = basic},
	tcap_user:send_prim(State#state.tco, {'TC','END',request,End}),

	{next_state, idle, State}.

handle_event(Event, StateName, StateData) ->
	error_logger:format("~w received unexpected event in state ~w: ~w~n",
			    [?MODULE, StateName, Event]),
	{next_state, StateName, StateData}.

handle_sync_event(Event, _From, StateName, StateData) ->
	error_logger:format("~w received unexpected sync_event in state ~w: ~w~n",
			    [?MODULE, StateName, Event]),
	{next_state, StateName, StateData}.

handle_info(Event, StateName, StateData) ->
	error_logger:format("~w received unexpected info in state ~w: ~w~n",
			    [?MODULE, StateName, Event]),
	{next_state, StateName, StateData}.

terminate(_Reason, _StateName, _State) ->
	ok.

code_change(_Old, StateName, State, _Extra) ->
	{ok, StateName, State}.

